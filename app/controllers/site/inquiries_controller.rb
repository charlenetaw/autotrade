class Site::InquiriesController < SiteController

  def new
    @inquiry = Inquiry.new
  end

  def create
    
    @inquiry = Inquiry.new(params[:inquiry])

    respond_to do |format| 
      if @inquiry.save
        format.js { render :action => "success" }
      else
        format.js { render :action => "error" }
      end
    end 

  end


end