class Site::BlogsController < SiteController

  def index
    @blogs = Blog.order("posted_date DESC")
  end

end