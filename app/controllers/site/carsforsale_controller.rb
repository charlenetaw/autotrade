class Site::CarsforsaleController < SiteController

  def index
    @vehicles = Post.order("updated_at DESC").paginate(:page => params[:vehicles], :per_page => 12)
  end

  def show
    # Cars for Sale
    @post = Post.find_by_slug!(params[:id])

    @car_photos = VehiclePhoto.where("post_id =?", @post.id)

    @seller = User.select("id, first_name, last_name, email, contact").where(:id => @post.user_id).last

    @inquiry = Inquiry.new

    # Featured Cars for Sale
    @featured_cars = Post.where("is_active =?", 1).where("is_featured =?", 1).order("id DESC").paginate(:page => params[:featured_cars], :per_page => 5)
    @posts = Post.select("id, title, slug").where("is_active =?", 1).order("id DESC").paginate(:page => params[:posts], :per_page => 10)

    @makers = Maker.select("name").order("name DESC")

    @related_listings = Post.where("maker_id =?", @post.maker_id).where("is_active =?", 1).where("id <>?", @post.id)

    @listing_cars = Post.select("id, title, slug").where("is_active =?", 1).order("id DESC").paginate(:page => params[:listing_cars], :per_page => 10)
    @brand_vehicles_prices = PriceList.select("maker_id, slug").order("slug DESC").group(:maker_id, :slug)
    @sale_by_brands = Post.select("maker_id").order("maker_id DESC").order("maker_id DESC").group(:maker_id)
    @sale_by_conditions = Post.select("condition_id").order("condition_id DESC").order("condition_id DESC").group(:condition_id)
  end

  
end