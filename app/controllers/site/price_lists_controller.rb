class Site::PriceListsController < SiteController

  def index
    @price_list = PriceList.find_by_slug!(params[:id])
  end

end