class Site::HomeController < SiteController

  def index
    @price_lists = PriceList.select("maker_id, slug").order("maker_id DESC").group(:maker_id, :slug).all

    @posts = Post.where("is_active =?", 1).order("created_at DESC").paginate(:page => params[:posts], :per_page => 4)

    @featured_cars = Post.where("is_active =?", 1).where("is_featured =?", 1).order("id DESC").paginate(:page => params[:featured_cars], :per_page => 3)

    @makers = Maker.select("id, name").order("name DESC")
  end

  def show

    # Brand Car List of Prices
    @lookup_slug = PriceList.find_by_slug!(params[:id])
    @price_lists = PriceList.where("slug =?", params[:id])

    # Create New Inquiry
    @inquiry = Inquiry.new

    # Side list 
    @listing_cars = Post.select("id, title, slug").where("is_active =?", 1).order("id DESC").paginate(:page => params[:listing_cars], :per_page => 10)
    @brand_vehicles_prices = PriceList.select("maker_id, slug").order("slug DESC").group(:maker_id, :slug)
    @sale_by_brands = Post.select("maker_id").order("maker_id DESC").order("maker_id DESC").group(:maker_id)
    @sale_by_conditions = Post.select("condition_id").order("condition_id DESC").order("condition_id DESC").group(:condition_id)
    
  end

end