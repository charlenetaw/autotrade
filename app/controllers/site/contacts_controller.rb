class Site::ContactsController < SiteController

  def index
    @admin = User.select("id, first_name, last_name, email, contact").where("role_id =?", 1).first
    @inquiry = Inquiry.new
    @inquiry_types = [['Inquiry For Cars','1'],['Auto Loan','2'],['Car Price Lists','3'],['Put your ads in our site', '4'],['Others','5']]
  end


end