class Admin::PostsController < AdminController

  def index
    @posts = Post.select("id, title, maker_id, vehicle_model_id, price, year_model_id").where("is_active =?", 1).order("id DESC").paginate(:page => params[:posts], :per_page => 10)
  end

  def new
    @post = Post.new
    @status = [['Active','1'],['Inactive','0']]
    @categories = Category.select("id, name").order("name ASC")
    @featured = [['Yes','1'],['No','0']]

    @makers = Maker.select("id, name").order("name ASC")
    @vehicle_models = VehicleModel.select("id, maker_id, name").order("name ASC")

    @conditions = Condition.select("id, name").order("name ASC")
    @vehicle_types = VehicleType.select("id, name").order("name ASC")

    @fuel_types = FuelType.select("id, name").order("name ASC")
    @transmissions = Transmission.select("id, name").order("name ASC")

    @year_model = YearModel.select("id, name").order("name ASC")
    @milages = Milage.select("id, name").order("name ASC")

    5.times { @post.vehicle_photos.build }
  end

  def create
    @post = Post.new(params[:post])
    @post.user_id = current_user.id

    if @post.save
      redirect_to "/admin/posts/#{@post.id}", notice: 'New Post was successfully created.'
    else
      redirect_to "new"
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = Post.find(params[:id])

    @status = [['Active','1'],['Inactive','0']]
    @categories = Category.select("id, name").order("name ASC")
    @featured = [['Yes','1'],['No','0']]

    @makers = Maker.select("id, name").order("name ASC")
    @vehicle_models = VehicleModel.select("id, maker_id, name").order("name ASC")

    @conditions = Condition.select("id, name").order("name ASC")
    @vehicle_types = VehicleType.select("id, name").order("name ASC")

    @fuel_types = FuelType.select("id, name").order("name ASC")
    @transmissions = Transmission.select("id, name").order("name ASC")

    @year_model = YearModel.select("id, name").order("name ASC")
    @milages = Milage.select("id, name").order("name ASC")

    5.times { @post.vehicle_photos.build } if  @post.vehicle_photos.blank?
  end

  def update
    @post = Post.find(params[:id])
    @post.delete_photo if params[:delete_attachment] ==1
    @post.user_id = current_user.id
    if @post.update_attributes(params[:post])
      redirect_to "/admin/posts/#{@post.id}", notice: 'Post was successfully updated.'
    else
      redirect_to "edit"
    end
  end

  def destroy
    @post = Post.destroy(params[:id]) 
    @post.destroy

    redirect_to "/admin/posts", notice: 'Post successfully deleted.'
  end

  def update_models
    # updates vehicle_model based on maker selected

    # vmodels = VehicleModel.select("id, maker_id, name").where(:maker_id => params[:maker_id])
    maker = Maker.find(params[:maker_id])
    @vehicle_models = maker.vehicle_models.map{|vm| [vm.name, vm.id]}.insert(0, "Please select a Model")
    
  end

end