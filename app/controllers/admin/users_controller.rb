class Admin::UsersController < AdminController

  def index
    @users = User.select("id, first_name, last_name, email, created_at").order("id DESC").paginate(:page => params[:users], :per_page => 10)
  end

  def show
    @user = User.find(params[:id])
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to "/admin/users" 
  end

end