class Admin::BlogsController < AdminController

  def index
    @blogs = Blog.select("id, title, content, posted_date").order("posted_date DESC").paginate(:page => params[:blogs], :per_page => 10)
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(params[:blog])
    if @blog.save
      redirect_to "/admin/blogs/#{@blog.id}", notice: 'New Blog was successfully created.'
    else
      redirect_to "new"
    end
  end

  def show
    @blog = Blog.find(params[:id])
  end

  def edit
    @blog = Blog.find(params[:id])
  end

  def update
    @blog = Blog.find(params[:id])
    @blog.delete_photo if params[:delete_attachment] ==1
    if @blog.update_attributes(params[:blog])
      redirect_to "/admin/blogs/#{@blog.id}", notice: 'Blog was successfully updated.'
    else
      redirect_to "edit"
    end
  end

  def destroy
    @blog = Blog.destroy(params[:id]) 
    @blog.destroy
    redirect_to "/admin/blogs"
  end

end