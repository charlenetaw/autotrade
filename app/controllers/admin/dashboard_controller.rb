class Admin::DashboardController < AdminController

  def index
    @inquiries = Inquiry.where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).count
    # @posts = Post.where("created_at =?", DateTime.now.to_date).count
    @posts = Post.where("is_active =?", 0).count
    @users = User.where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).count

    @recent_activities = Post.select("id, title, created_at, user_id").where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day + 14.days).order("created_at DESC").paginate(:page => params[:recent_activities], :per_page => 10)
  end

  def today_inquiries
    @inquiries = Inquiry.select("id, sender, email, message, inquiry_type, created_at").where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).paginate(:page => params[:inquiries], :per_page => 10)
  end

  def today_posts
    @posts = Post.select("id, title, user_id, maker_id, vehicle_model_id, created_at").where("is_active =?", 0).paginate(:page => params[:posts], :per_page => 10)
  end

  def today_users
    @users = User.select("id, first_name, last_name, email, created_at").where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).paginate(:page => params[:users], :per_page => 10)
  end

end