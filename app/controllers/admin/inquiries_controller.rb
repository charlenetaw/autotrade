class Admin::InquiriesController < AdminController

  def index
    @inquiries = Inquiry.select("id, sender, email, created_at").where("user_id =?", current_user.id).order("created_at DESC").paginate(:page => params[:inquiries], :per_page => 10)
  end

  def show
    @inquiry = Inquiry.find(params[:id])
  end

  def destroy
    @inquiry = Inquiry.find(params[:id])
    @inquiry.destroy

    redirect_to "/admin/inquiries", notice: 'Inquiry successfully deleted.'
  end

end