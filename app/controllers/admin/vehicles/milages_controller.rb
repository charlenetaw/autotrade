class Admin::Vehicles::MilagesController < Admin::VehiclesController
  
  def index
    @milages = Milage.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:milages], :per_page => 10)
  end

  def new
    @milage = Milage.new
  end

  def create
    @milage = Milage.new(params[:milage])
    if @milage.save
      redirect_to "/admin/vehicles/milages/#{@milage.id}", notice: 'Milage successfully created.'
    else
      render "new"
    end
  end

  def show
    @milage = Milage.find(params[:id])
  end

  def edit
    @milage = Milage.find(params[:id])
  end

  def update
    @milage = Milage.find(params[:id])
    if @milage.update_attributes(params[:milage])
      redirect_to "/admin/vehicles/milages/#{@milage.id}", notice: 'Milage successfully updated.'
    else
      render "edit"
    end
  end
  
end