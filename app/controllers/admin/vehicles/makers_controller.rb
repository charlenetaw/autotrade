class Admin::Vehicles::MakersController < Admin::VehiclesController
  
  def index
    @makers = Maker.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:makers], :per_page => 10)
  end

  def new
    @maker = Maker.new
  end

  def create
    @maker = Maker.new(params[:maker])
    if @maker.save
      redirect_to "/admin/vehicles/makers/#{@maker.id}", notice: 'Maker successfully created.'
    else
      render "new"
    end
  end

  def show
    @maker = Maker.find(params[:id])
  end

  def edit
    @maker = Maker.find(params[:id])
  end

  def update
    @maker = Maker.find(params[:id])
    if @maker.update_attributes(params[:maker])
      redirect_to "/admin/vehicles/makers/#{@maker.id}", notice: 'Maker successfully updated.'
    else
      render "edit"
    end
  end
  
end