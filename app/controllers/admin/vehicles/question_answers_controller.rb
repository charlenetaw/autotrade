class Admin::Vehicles::QuestionAnswersController < Admin::VehiclesController
  
  def index
    @question_answers = QuestionAnswer.select("id, question, answer, updated_at").order("id ASC").paginate(:page => params[:question_answers], :per_page => 10)
  end

  def new
    @question_answer = QuestionAnswer.new
  end

  def create
    @question_answer = QuestionAnswer.new(params[:question_answer])
    if @question_answer.save
      redirect_to "/admin/vehicles/question_answers/#{@question_answer.id}", notice: 'QuestionAnswer successfully created.'
    else
      render "new"
    end
  end

  def show
    @question_answer = QuestionAnswer.find(params[:id])
  end

  def edit
    @question_answer = QuestionAnswer.find(params[:id])
  end

  def update
    @question_answer = QuestionAnswer.find(params[:id])
    if @question_answer.update_attributes(params[:question_answer])
      redirect_to "/admin/vehicles/question_answers/#{@question_answer.id}", notice: 'QuestionAnswer successfully updated.'
    else
      render "edit"
    end
  end
  
end