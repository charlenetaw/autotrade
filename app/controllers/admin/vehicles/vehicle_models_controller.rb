class Admin::Vehicles::VehicleModelsController < Admin::VehiclesController
  
  def index
    @vehicle_models = VehicleModel.select("id, name, maker_id, description, updated_at").order("name ASC").paginate(:page => params[:vehicle_models], :per_page => 10)
  end

  def new
    @vehicle_model = VehicleModel.new
    lookups
  end

  def create
    @vehicle_model = VehicleModel.new(params[:vehicle_model])
    if @vehicle_model.save
      redirect_to "/admin/vehicles/vehicle_models/#{@vehicle_model.id}", notice: 'Vehicle Model successfully created.'
    else
      render "new"
    end
  end

  def show
    @vehicle_model = VehicleModel.find(params[:id])
  end

  def edit
    lookups
    @vehicle_model = VehicleModel.find(params[:id])
  end

  def update
    @vehicle_model = VehicleModel.find(params[:id])
    if @vehicle_model.update_attributes(params[:vehicle_model])
      redirect_to "/admin/vehicles/vehicle_models/#{@vehicle_model.id}", notice: 'Vehicle Model successfully updated.'
    else
      render "edit"
    end
  end
  
  private
  def lookups
    @makers =  Maker.select("id, name").order("name ASC")
  end

end