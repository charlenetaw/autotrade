class Admin::Vehicles::VehicleTypesController < Admin::VehiclesController
  
  def index
    @vehicle_types = VehicleType.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:vehicle_types], :per_page => 10)
  end

  def new
    @vehicle_type = VehicleType.new
  end

  def create
    @vehicle_type = VehicleType.new(params[:vehicle_type])
    if @vehicle_type.save
      redirect_to "/admin/vehicles/vehicle_types/#{@vehicle_type.id}", notice: 'Vehicle Type successfully created.'
    else
      render "new"
    end
  end

  def show
    @vehicle_type = VehicleType.find(params[:id])
  end

  def edit
    @vehicle_type = VehicleType.find(params[:id])
  end

  def update
    @vehicle_type = VehicleType.find(params[:id])
    if @vehicle_type.update_attributes(params[:vehicle_type])
      redirect_to "/admin/vehicles/vehicle_types/#{@vehicle_type.id}", notice: 'Vehicle Type successfully updated.'
    else
      render "edit"
    end
  end
  
end