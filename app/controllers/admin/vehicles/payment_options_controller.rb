class Admin::Vehicles::PaymentOptionsController < Admin::VehiclesController
  
  def index
    @payment_options = PaymentOption.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:payment_options], :per_page => 10)
  end

  def new
    @payment_option = PaymentOption.new
  end

  def create
    @payment_option = PaymentOption.new(params[:payment_option])
    if @payment_option.save
      redirect_to "/admin/vehicles/payment_options/#{@payment_option.id}", notice: 'Payment Option successfully created.'
    else
      render "new"
    end
  end

  def show
    @payment_option = PaymentOption.find(params[:id])
  end

  def edit
    @payment_option = PaymentOption.find(params[:id])
  end

  def update
    @payment_option = PaymentOption.find(params[:id])
    if @payment_option.update_attributes(params[:payment_option])
      redirect_to "/admin/vehicles/payment_options/#{@payment_option.id}", notice: 'Payment Option successfully updated.'
    else
      render "edit"
    end
  end
  
end