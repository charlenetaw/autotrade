class Admin::Vehicles::ConditionsController < Admin::VehiclesController
  
  def index
    @conditions = Condition.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:conditions], :per_page => 10)
  end

  def new
    @condition = Condition.new
  end

  def create
    @condition = Condition.new(params[:condition])
    if @condition.save
      redirect_to "/admin/vehicles/conditions/#{@condition.id}", notice: 'Condition successfully created.'
    else
      render "new"
    end
  end

  def show
    @condition = Condition.find(params[:id])
  end

  def edit
    @condition = Condition.find(params[:id])
  end

  def update
    @condition = Condition.find(params[:id])
    if @condition.update_attributes(params[:condition])
      redirect_to "/admin/vehicles/conditions/#{@condition.id}", notice: 'Condition successfully updated.'
    else
      render "edit"
    end
  end
  
end