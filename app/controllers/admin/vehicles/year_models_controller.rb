class Admin::Vehicles::YearModelsController < Admin::VehiclesController
  
  def index
    @year_models = YearModel.select("id, name, updated_at").order("name ASC").paginate(:page => params[:year_models], :per_page => 10)
  end

  def new
    @year_model = YearModel.new
  end

  def create
    @year_model = YearModel.new(params[:year_model])
    if @year_model.save
      redirect_to "/admin/vehicles/year_models/#{@year_model.id}", notice: 'Year Model Type successfully created.'
    else
      render "new"
    end
  end

  def show
    @year_model = YearModel.find(params[:id])
  end

  def edit
    @year_model = YearModel.find(params[:id])
  end

  def update
    @year_model = YearModel.find(params[:id])
    if @year_model.update_attributes(params[:year_model])
      redirect_to "/admin/vehicles/year_models/#{@year_model.id}", notice: 'Year Model successfully updated.'
    else
      render "edit"
    end
  end
  
end