class Admin::Vehicles::PriceListsController < Admin::VehiclesController
  
  def index
    @price_lists = PriceList.select("id, maker_id, variant, amount, lto_reg").order("maker_id ASC").paginate(:page => params[:price_lists], :per_page => 10)
  end

  def new
    lookups
    @price_list = PriceList.new
  end

  def create
    @price_list = PriceList.new(params[:price_list])
    if @price_list.save
      redirect_to "/admin/vehicles/price_lists/#{@price_list.id}", notice: 'PriceList successfully created.'
    else
      render "new"
    end
  end

  def show
    @price_list = PriceList.find(params[:id])
  end

  def edit
    lookups
    @price_list = PriceList.find(params[:id])
  end

  def update
    @price_list = PriceList.find(params[:id])
    if @price_list.update_attributes(params[:price_list])
      redirect_to "/admin/vehicles/price_lists/#{@price_list.id}", notice: 'PriceList successfully updated.'
    else
      render "edit"
    end
  end

  private
  def lookups
    @makers =  Maker.select("id, name").order("name DESC")
  end
  
end