class Admin::Vehicles::TransmissionsController < Admin::VehiclesController
  
  def index
    @transmissions = Transmission.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:transmissions], :per_page => 10)
  end

  def new
    @transmission = Transmission.new
  end

  def create
    @transmission = Transmission.new(params[:transmission])
    if @transmission.save
      redirect_to "/admin/vehicles/transmissions/#{@transmission.id}", notice: 'Transmission successfully created.'
    else
      render "new"
    end
  end

  def show
    @transmission = Transmission.find(params[:id])
  end

  def edit
    @transmission = Transmission.find(params[:id])
  end

  def update
    @transmission = Transmission.find(params[:id])
    if @transmission.update_attributes(params[:transmission])
      redirect_to "/admin/vehicles/transmissions/#{@transmission.id}", notice: 'Transmission successfully updated.'
    else
      render "edit"
    end
  end
  
end