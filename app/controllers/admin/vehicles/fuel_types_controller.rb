class Admin::Vehicles::FuelTypesController < Admin::VehiclesController
  
  def index
    @fuel_types = FuelType.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:fuel_types], :per_page => 10)
  end

  def new
    @fuel_type = FuelType.new
  end

  def create
    @fuel_type = FuelType.new(params[:fuel_type])
    if @fuel_type.save
      redirect_to "/admin/vehicles/fuel_types/#{@fuel_type.id}", notice: 'Fuel Type successfully created.'
    else
      render "new"
    end
  end

  def show
    @fuel_type = FuelType.find(params[:id])
  end

  def edit
    @fuel_type = FuelType.find(params[:id])
  end

  def update
    @fuel_type = FuelType.find(params[:id])
    if @fuel_type.update_attributes(params[:fuel_type])
      redirect_to "/admin/vehicles/fuel_types/#{@fuel_type.id}", notice: 'Fuel Type successfully updated.'
    else
      render "edit"
    end
  end
  
end