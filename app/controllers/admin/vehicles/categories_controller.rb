class Admin::Vehicles::CategoriesController < Admin::VehiclesController
  
  def index
    @categories = Category.select("id, name, description, updated_at").order("name ASC").paginate(:page => params[:categories], :per_page => 10)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      redirect_to "/admin/vehicles/categories/#{@category.id}", notice: 'Category successfully created.'
    else
      render "new"
    end
  end

  def show
    @category = Category.find(params[:id])
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      redirect_to "/admin/vehicles/categories/#{@category.id}", notice: 'Category successfully updated.'
    else
      render "edit"
    end
  end
  
end