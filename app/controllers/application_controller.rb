class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_devise_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    if (current_user.role_id == 1)
      "/admin/dashboard"
    elsif (current_user.role_id == 2)
      "/member/dashboard"
    else
      "/"
    end
  end

  protected

  def configure_devise_permitted_parameters
    registration_params = [:email, :first_name, :last_name, :password, :password_confirmation]
    registration_update_params = [:email, :first_name, :last_name, :middle_name, :contact, :photo, :password, :password_confirmation]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) { 
        |u| u.permit(registration_update_params << :current_password)
      }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) { 
        |u| u.permit(registration_params) 
      }
    end
  end


end
