class Member::DashboardController < MemberController

  def index
    @inquiries = Inquiry.where('user_id =?', current_user.id).where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).count
    @pending_posts = Post.where("user_id =?", current_user.id).where("is_active =?", 0).count
    @total_posts = Post.where("user_id =?", current_user.id).count
    @posted_cars = Post.where("user_id =?", current_user.id).where("is_active =?", 1).count

    @recent_activities = Post.select("id, title, created_at, user_id").where("user_id =?", current_user.id).where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day + 14.days).order("created_at DESC").paginate(:page => params[:recent_activities], :per_page => 10)
  end

  def today_inquiries
    @inquiries = Inquiry.select("id, sender, email, message, inquiry_type, created_at").where('user_id =?', current_user.id).where('created_at BETWEEN ? AND ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day).paginate(:page => params[:inquiries], :per_page => 10)
  end

  def pending_posts
    @posts = Post.select("id, title, user_id, maker_id, vehicle_model_id, created_at").where('user_id =?', current_user.id).where("is_active =?", 0).paginate(:page => params[:posts], :per_page => 10)
  end

  def total_posts
    @posts = Post.select("id, title, user_id, maker_id, vehicle_model_id, created_at").where('user_id =?', current_user.id).paginate(:page => params[:posts], :per_page => 10)
  end

  def posted_cars
    @posts = Post.select("id, title, user_id, maker_id, vehicle_model_id, created_at").where('user_id =?', current_user.id).where("is_active =?", 1).paginate(:page => params[:posts], :per_page => 10)
  end

end