module ApplicationHelper

  def format_date(date)
    return date.blank? ? "" : date.strftime("%B %d, %Y")
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def for_admin_only?
    true if current_user.role_id == 1
  end
  
end
