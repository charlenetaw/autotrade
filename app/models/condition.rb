class Condition < ActiveRecord::Base

  attr_accessible :name, :description, :slug
  
  # Relationship
  has_many :posts
end