class Inquiry < ActiveRecord::Base

  # 1 = Inquiry for cars
  # 2 = Auto Loan
  # 3 = Price Lists
  # 4 = Ads
  # 5 = Others
  attr_accessible :sender, :email, :message, :inquiry_type, :user_id
  
  def get_inquiry_type
    if self.inquiry_type == 1 
      "Inquiry For Cars"
    elsif self.inquiry_type == 2
      "Auto Loan"
    elsif self.inquiry_type == 3
      "Price Lists"
    elsif self.inquiry_type == 4
      "Ads"
    else
      "Others"
    end
      
  end

end