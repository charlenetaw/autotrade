class Maker < ActiveRecord::Base

  attr_accessible :name, :description, :slug
  
  #Relationship
  has_many :price_lists
  accepts_nested_attributes_for :price_lists, :reject_if => :all_blank, :allow_destroy => true

  has_many :vehicle_models
  has_many :posts
  
end