class VehicleModel < ActiveRecord::Base

  attr_accessible :maker_id, :name, :description, :slug
  
  # Relationship
  belongs_to :maker
  has_many :posts
end