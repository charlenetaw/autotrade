class PriceList < ActiveRecord::Base

  attr_accessible :maker_id, :variant, :amount, :lto_reg, :slug
  # Relationship
  belongs_to :maker
end