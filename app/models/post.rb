class Post < ActiveRecord::Base

  attr_accessible :title, :price, :maker_id, :vehicle_model_id, :condition_id, :vehicle_type_id
  attr_accessible :fuel_type_id, :transmission_id, :year_model_id, :plate_ending, :milage_id
  attr_accessible :payment_option_id, :content, :slug, :excerpt, :is_featured, :photo
  attr_accessible :category_id, :location, :user_id, :is_active, :vehicle_photos_attributes
  
  has_attached_file :photo, {
    :default_url => "/assets/missing.png",
    :path => "assets/:id/:basename.:extension"
  }.merge(PAPERCLIP_STORAGE_OPTIONS)

  validates_attachment :photo, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

  # Relationship
  has_many :vehicle_photos
  accepts_nested_attributes_for :vehicle_photos

  belongs_to :year_model
  belongs_to :condition
  belongs_to :vehicle_type
  belongs_to :transmission
  belongs_to :fuel_type
  belongs_to :milage
  belongs_to :maker
  belongs_to :vehicle_model
  belongs_to :category

  belongs_to :user

  def get_status
    self.is_active == 1 ? "Posted" : "Pending"
  end

  def get_featured
    self.is_featured == 1 ? "Featured Car" : ""
  end


end