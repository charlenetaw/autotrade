function sameheight(){
  $('#right').css('minHeight', '0px');
  $('#left').css('minHeight', '0px');
  var sec_height = $('#listofmembers').height();
  $('#right').css('minHeight', sec_height + 50);
  $('#left').css('minHeight', sec_height + 30);
}

function recall_sameheight(){
  $('#accordion-membership .collapse').each(function(){
    $(this).on('hidden.bs.collapse', function () {
      sameheight();
    });
  });
};

function toggleChevron(e) {
  $(e.target)
    .prev('.panel-heading')
    .find("i.indicator")
    .toggleClass('fa-chevron-right fa-chevron-down');
}

$(document).on("click", '.go-to-top', function(event) {
  event.preventDefault();
  return $('body').animate({
    scrollTop: $("#navigation").offset().top
  }, 500);
});

$(window).load(function(){
  sameheight();
  recall_sameheight();
  $('#accordion').on('hide.bs.collapse', toggleChevron);
  $('#accordion').on('show.bs.collapse', toggleChevron);
});

$(document).ready(function(){
  var url = document.location.toString();
  if (url.match('#')) {
      $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
  } 


  // handles the carousel thumbnails
  $('[id^=carousel-selector-]').click( function(){
    var id_selector = $(this).attr("id");
    var id = id_selector.substr(id_selector.length -1);
    id = parseInt(id);
    $('#myCarousel').carousel(id);
    $('[id^=carousel-selector-]').removeClass('selected');
    $(this).addClass('selected');
  });

  // when the carousel slides, auto update
  $('#myCarousel').on('slid', function (e) {
    var id = $('.item.active').data('slide-number');
    id = parseInt(id);
    $('[id^=carousel-selector-]').removeClass('selected');
    $('[id^=carousel-selector-'+id+']').addClass('selected');
  });
});
