$(document).on "click", '.submit-password', (event) ->
  event.preventDefault()
  if is_not_blank('.required') && is_password_correct('.password', 8)
    $('form').submit()