$(document).on "click", '.submit-msg', (event) ->
  if is_not_blank($('#contact-anchor').find('.required'))
    $('#new_inquiry').submit() 
    $('#contact-anchor').fadeOut("fast")
    $('#thank-you').fadeIn("slow")
    $('.contact-field').val('')
$(document).on "click", '.reload-contact', (event) ->
  $('#thank-you').addClass("hidden").fadeIn( "slow" )
  $('#contact-anchor').removeClass("hidden").fadeIn( "slow" )