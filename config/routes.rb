Autotrade::Application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks" } do
    get 'users/sign_out' => 'devise/sessions#destroy'
  end

  root 'site/home#index'

  namespace :admin do 
    resources :dashboard do 
      member do
        get 'today_inquiries'
        get 'today_posts'
        get 'today_users'
      end
    end

    resources :posts do
      collection do 
        get "update_models"
      end
    end
    
    namespace :vehicles do 
      resources :categories
      resources :vehicle_types
      resources :vehicle_models
      resources :makers
      resources :fuel_types
      resources :transmissions
      resources :conditions
      resources :milages
      resources :payment_options
      resources :year_models

      resources :price_lists
      resources :question_answers
    end

    resources :inquiries 

    resources :users
    resources :blogs

  end

  # Member
  namespace :member do
    resources :dashboard do 
      member do
        get 'today_inquiries'
        get 'pending_posts'
        get 'total_posts'
        get 'posted_cars'
      end
    end
    resources :posts do 
      collection do 
        get "update_models"
      end
    end
    resources :inquiries
  end

  # SITE
  get "blogs", to: 'site/blogs#index'
  get "blogs/:id", to: 'site/blogs#show'
  get "/:id", to: 'site/home#show'
  get "carsforsale/:id", to: 'site/carsforsale#show'

  namespace :site do 
    resources :inquiries, :only => [:create]
    resources :carsforsale, :only => [:index]
    resources :contacts, :only => [:index]
    resources :autotrade_ads, :only => [:index]
    resources :question_answers, :only => [:index]
    resources :dealerslisting, :only =>[:show]
  end

  
  
end
