# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Autotrade::Application.config.secret_key_base = 'a8e8f1ab4d5b4ff53d30764d53e37bb02a6be2ec01b06ad4acf63be5da92a6652c133213feca552dfd71231c31b2493e7bf459ccdb222b3af8f078bf27611cb6'
