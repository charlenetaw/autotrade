# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.delete_all
ActiveRecord::Base.connection.execute("ALTER SEQUENCE users_id_seq RESTART WITH 1;")
User.create(:email=> "developer@autotrade.com", :password=>"password",:first_name=>"Abe",:last_name=>"Dumlao", :role_id=>1, :contact => "09175287233")
User.create(:email=> "tawcharlene@gmail.com", :password=>"charlenetaw",:first_name=>"Charlene",:last_name=>"Taw", :middle_name=>"Lopez",:role_id=>1, :contact => "09354281176")
