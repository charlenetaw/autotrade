class AddOmniauth < ActiveRecord::Migration
  def change
    add_column :users, :user_name, :string
    add_column :users, :facebook_link, :string
    add_column :users, :provider, :string
    add_column :users, :uid, :string
  end
end