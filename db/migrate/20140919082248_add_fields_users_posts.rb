class AddFieldsUsersPosts < ActiveRecord::Migration
  def change
    add_column :posts, :is_active, :integer
    add_attachment :users, :photo
  end
end