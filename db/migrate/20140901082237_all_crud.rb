class AllCrud < ActiveRecord::Migration
  def change
    # Category
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Vehicle Types
    create_table :vehicle_types do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Maker
    create_table :makers do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Fuel Type
    create_table :fuel_types do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Transmission
    create_table :transmissions do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Condition
    create_table :conditions do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Milage
    create_table :milages do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

    # Payment Option
    create_table :payment_options do |t|
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

  end
end