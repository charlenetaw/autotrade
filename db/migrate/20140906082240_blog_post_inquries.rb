class BlogPostInquries < ActiveRecord::Migration
  def change
    # Blogs
    create_table :blogs do |t|
      t.string :title
      t.text :content
      t.integer :is_active
      t.text :excerpt
      t.datetime :posted_date
      t.attachment :photo
      t.text :slug
      t.timestamps
    end

    # Posts
    create_table :posts do |t|
      t.string :title
      t.string :price
      t.integer :maker_id
      t.integer :vehicle_model_id
      t.integer :condition_id
      t.integer :vehicle_type_id
      t.integer :fuel_type_id
      t.integer :transmission_id
      t.integer :year_model
      t.integer :plate_ending
      t.integer :milage_id
      t.integer :payment_option_id
      t.text :content
      t.text :slug
      t.text :excerpt
      t.timestamps
    end

    # Inquries
    create_table :inquries do |t|
      t.string :sender
      t.string :email
      t.text :message
      t.timestamps
    end

    # Vehicle Models
    create_table :vehicle_models do |t|
      t.integer :maker_id
      t.string :name
      t.text :description
      t.text :slug
      t.timestamps
    end

  end
end