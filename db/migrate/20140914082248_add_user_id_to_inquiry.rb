class AddUserIdToInquiry < ActiveRecord::Migration
  def change
    add_column :inquiries, :user_id, :integer
    rename_column :inquiries, :type, :inquiry_type
  end
end