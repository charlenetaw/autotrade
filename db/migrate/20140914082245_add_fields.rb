class AddFields < ActiveRecord::Migration
  def change

    add_column :posts, :location, :text
    add_column :posts, :user_id, :integer
    
    add_column :users, :contact, :string

  end
end