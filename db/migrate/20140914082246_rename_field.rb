class RenameField < ActiveRecord::Migration
  def change
    rename_column :posts, :year_model, :year_model_id
  end
end