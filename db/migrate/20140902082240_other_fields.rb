class OtherFields < ActiveRecord::Migration
  def change
    # Price Listing
    create_table :price_lists do |t|
      t.string :maker_id
      t.string :model
      t.string :variant
      t.text :amount
      t.text :lto_reg
      t.text :slug
      t.timestamps
    end

    # FAQs
    create_table :question_answers do |t|
      t.text :question
      t.text :answer
      t.text :slug
      t.timestamps
    end

  end
end