class VehiclePhotoFields < ActiveRecord::Migration
  def change

    create_table :vehicle_photos do |t|
      t.integer :post_id
      t.attachment :photo
      t.timestamps
    end

    add_attachment :posts, :photo
    add_column :posts, :category_id, :integer

  end
end