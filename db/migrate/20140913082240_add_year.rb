class AddYear < ActiveRecord::Migration
  def change

    create_table :year_models do |t|
      t.string :name
      t.timestamps
    end
    
    add_column :posts, :is_featured, :integer

  end
end