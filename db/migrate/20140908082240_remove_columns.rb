class RemoveColumns < ActiveRecord::Migration
  def change
    remove_column :price_lists, :model

    add_column :inquries, :type, :integer

    rename_table :inquries, :inquiries
  end
end